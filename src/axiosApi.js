import axios from 'axios';

const axiosApi = axios.create ({
    baseURL: 'https://my-project-app-number-1-default-rtdb.firebaseio.com'
});

export default axiosApi;