import {BrowserRouter, Switch, Route} from "react-router-dom";
import Layout from "./UI/Layout/Layout";
import Page from "./Container/Page/Page";
import EditPages from "./Container/EditPages/EditPages";
import './App.css';

function App() {
  return (
    <div className="App">
        <div className="top"/>
        <div className="block">
            <h1 className="mainTitle">Static Pages</h1>
            <div>
                <BrowserRouter>
                    <Layout>
                        <div className="switch">
                            <Switch>
                                <Route path="/" exact component={Page}/>
                                <Route path="/pages/:category" component={Page}/>
                                <Route path="/admin" component={EditPages}/>
                                <Route render={() => <h1>Not found</h1>}/>
                            </Switch>
                        </div>
                    </Layout>
                </BrowserRouter>
            </div>
        </div>

    </div>
  );
}

export default App;
