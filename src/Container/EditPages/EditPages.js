import React from 'react';
import axiosApi from "../../axiosApi";
import {useState} from "react";
import {Grid, makeStyles, MenuItem, Paper, TextField} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2)
    }
}))

const EditPages = ({history}) => {

    const classes = useStyles();

    const [add, setAdd] = useState({

        category: {
            title: '',
            content: '',
    }});


    const onInputChange = e => {

        const {name, value} = e.target;

        setAdd(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createContent = async e => {
        e.preventDefault();
        await axiosApi.post('/post.json', {...add});
        history.replace('/');
    };

    return (
        <div className="add">
            <Grid container direction="column" spacing={2} className={classes.root}>
                <h2 className="titleH2">Edit pages</h2>
            </Grid>
            <Grid item>
                <Paper className={classes.paper}>
                    <Grid container direction="column" spacing={2} component="form" onSubmit={createContent}>
                        <Grid item>
                            <TextField
                                required
                                select
                                label="category"
                                name="category"
                                value={add.category}
                                onChange={onInputChange}
                                fullWidth
                                variant="outlined"
                            >
                                <MenuItem value=""><em>Select a category</em></MenuItem>
                                <MenuItem value="about"><em>About</em></MenuItem>
                                <MenuItem value="contacts"><em>Contacts</em></MenuItem>
                                <MenuItem value="divisions"><em>Divisions</em></MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item>
                            <TextField
                                required
                                label="title"
                                name="title"
                                value={add.title}
                                onChange={onInputChange}
                                fullWidth
                                variant="outlined"
                            >
                            </TextField>
                        </Grid>
                        <Grid item>
                            <TextField
                                required
                                label="content"
                                name="content"
                                multiline
                                value={add.content}
                                onChange={onInputChange}
                                fullWidth
                                variant="outlined"
                            >
                            </TextField>
                        </Grid>
                        <Grid item>
                            <button>Save</button>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </div>
    );
};

export default EditPages;