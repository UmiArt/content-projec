import React from 'react';
import {useState, useEffect} from "react";
import './Page.css';
import axiosApi from "../../axiosApi";

const Page = ({match}) => {

    const [allContents, setAllContents] = useState(null);

    useEffect(() => {

        const fetchData = async () => {

            console.log(match.params.category)

            let url = 'https://my-project-app-number-1-default-rtdb.firebaseio.com/post.json';

            if(match.params.category) {
                url +=`https://my-project-app-number-1-default-rtdb.firebaseio.com/post/${match.params.category}.json`
            }
            const response = await axiosApi.get(url);
            const rest = response.data
            setAllContents(rest);
        }

        fetchData().catch(e => console.error(e));
    },[match.params.category]);


    return allContents && (
        <div className="allCont">
            <h2 className="titleH2">All contents</h2>
            {Object.keys(allContents).map(newP => (
                <div className="content" key={newP.toString()}>
                    <p>Title : {allContents[newP].title}</p>
                    <p>Content : {allContents[newP].content}</p>
                </div>
            ))};
        </div>
    );
};

export default Page;