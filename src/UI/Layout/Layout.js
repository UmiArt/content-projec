import React from 'react';
import {Link, NavLink} from "react-router-dom";
import './Layout.css';
import { MenuItem} from "@material-ui/core";
import {CATEGORIES} from "../../content";

const Layout = ({children}) => {
    return (
        <>
            <header>
                    {CATEGORIES.map(category => (
                        <MenuItem key={category.id}
                                  component={Link}
                                  to={"/post/" + category.id}
                        >
                                  {category.title}
                        </MenuItem>
                    ))}
                <li><NavLink to="/admin" activeStyle={{color:'steelblue'}}>Admin</NavLink></li>
            </header>
            <main className="Layout-Content">{children}</main>
        </>
    );
};

export default Layout;